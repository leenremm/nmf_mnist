# NMF Decomposition for the MNIST dataset

## About

Author: Leen Remmelzwaal

Written: July 2019

License: CC BY

## Description

A Python script to run NMF deconstruction and reconstruction on the MNIST dataset.

![picture](nmf_process.png)

## Getting Started

See `requirements.txt`

## Getting Started

Run the following in the Windows command line:

```
python nmf.py
```
